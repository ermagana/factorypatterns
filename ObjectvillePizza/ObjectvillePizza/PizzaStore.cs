﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectvillePizza.Pizzas;
using ObjectvillePizza.Pizzas.NY;
using ObjectvillePizza.Pizzas.Chicago;

namespace ObjectvillePizza
{
    public class PizzaStore
    {
        public Pizza orderPizza(string store, string type)
        {
            Pizza pizza = null;

            switch (store.ToUpper())
            {
                case "NY":
                    switch (type.ToUpper())
                    {
                        case "CHEESE":
                            pizza = new NYStyleCheesePizza();
                            break;
                        case "VEGGIE":
                            pizza = new NYStyleVeggiePizza();
                            break;
                        case "PEPPERONI":
                            pizza = new NYStylePepperoniPizza();
                            break;
                        case "CLAM":
                            pizza = new NYStyleClamPizza();
                            break;
                        default:
                            throw new Exception("Unknown pizza request");
                    }
                    break;
                case "CHICAGO":
                    switch (type.ToUpper())
                    {
                        case "CHEESE":
                            pizza = new ChicagoStyleCheesePizza();
                            break;
                        case "VEGGIE":
                            pizza = new ChicagoStyleVeggiePizza();
                            break;
                        case "PEPPERONI":
                            pizza = new ChicagoStylePepperoniPizza();
                            break;
                        case "CLAM":
                            pizza = new ChicagoStylePepperoniPizza();
                            break;
                        default:
                            throw new Exception("Unknown pizza request");
                    }
                    break;
                default:
                    throw new Exception("Unknown store");
            }

            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();

            return pizza;
        }
    }
}
