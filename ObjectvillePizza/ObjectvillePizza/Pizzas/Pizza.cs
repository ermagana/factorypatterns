﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectvillePizza.Pizzas
{
    public class Pizza
    {
        public void prepare()
        {
            Console.WriteLine("\tPreparing {0} pizza", this.GetType().Name.Replace("Pizza", ""));
        }

        public void bake()
        {
            Console.WriteLine("\tBaking {0} pizza", this.GetType().Name.Replace("Pizza", ""));
        }

        public void cut()
        {
            Console.WriteLine("\tCutting {0} pizza", this.GetType().Name.Replace("Pizza", ""));
        }

        public void box()
        {
            Console.WriteLine("\tBoxing {0} pizza", this.GetType().Name.Replace("Pizza", ""));
        }
    }
}
