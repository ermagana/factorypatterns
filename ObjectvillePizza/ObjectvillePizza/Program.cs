﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using ObjectvillePizza.Pizzas;

namespace ObjectvillePizza
{
    class Program
    {
        static void Main(string[] args)
        {
            var store = new PizzaStore();

            Console.WriteLine("Welcome to Objectville Pizza");
            help();

            Console.WriteLine("Select your store \n\tNY\n\tChicago");
            var storeRequest = Console.ReadLine();

            Console.WriteLine("\nPlace your order:");
            var pizzaRequest = Console.ReadLine();

            while(pizzaRequest.ToUpper() != "EXIT"){
                try
                {
                    store.orderPizza(storeRequest, pizzaRequest);
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0}", e.Message);
                    if (e.Message == "Unknown store")
                    {
                        Console.WriteLine("Select your store \n\tNY\n\tChicago");
                        storeRequest = Console.ReadLine();
                    }
                }
                Console.WriteLine("\nPlace another order:");
                pizzaRequest = Console.ReadLine();
            }            
        }

        static void help()
        {
            Console.WriteLine("These are your pizza options:");
            var pizzaOptions = Assembly.GetExecutingAssembly().GetTypes()
                            .Where(typ => typ.IsSubclassOf(typeof(Pizza)) && typ.Namespace.EndsWith("Pizzas") );
            foreach(var pizza in pizzaOptions){
                Console.WriteLine("\t{0}", pizza.Name.Replace("Pizza", ""));
            }

            Console.WriteLine("Type exit when finished");
        }
    }
}
